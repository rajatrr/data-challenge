# README #
# [How to write .md file](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

# [Collection of datasets](https://archive.ics.uci.edu/ml/datasets.html)


DataSets around which problem can be created:

* [Census data set](https://archive.ics.uci.edu/ml/datasets/Census+Income)

Algo Problems:

* [Coding the perceptron](https://datasciencelab.wordpress.com/2014/01/10/machine-learning-classics-the-perceptron/)

# Analytics:
* Descriptive Statistics on the Data Set
* Exploratory Data Analytics and Data Exploration on Data Set

# Programming